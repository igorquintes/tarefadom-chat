function enviarMsg(event){
    event.preventDefault();
    // botão enviar
    let input = document.querySelector(".Text");
    let escopo = document.querySelector(".DivHis");
    let texto = document.createElement("p");
    texto.innerText = input.value;
    escopo.appendChild(texto);

    // botão deletar
    let btnDelete = document.createElement("button");
    btnDelete.innerText = "Deletar";
    btnDelete.className = "Delete";
    texto.appendChild(btnDelete);

}

function deletarMsg(event) {
    if (event.target.classList.contains("Delete")) {
        let mensagem = event.target.parentNode;
        mensagem.remove();
    }
}

let btn_enviar = document.querySelector(".Send");
btn_enviar.addEventListener("click", enviarMsg);

let btn_deletar = document.querySelector(".DivHis");
btn_deletar.addEventListener("click", deletarMsg);


